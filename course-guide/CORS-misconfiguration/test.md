# 16.5 Verify that untrusted data is not used within crossdomain resource sharing (CORS) to protect against arbitrary remote content.

### CORS misconfiguration

CORS defines a way in which a browser and server can interact to determine whether or not it is safe to allow 
the cross-origin request. It allows for more freedom and functionality than purely same-origin requests.

We find a lot of applications these days that have CORS misconfigured.
This misconfiguration allows to make XHR get requests on an authenticated users behalf
and steal sensitive information from the server!

### Start the application

The following steps describe how to start the application either from source or from the owasp-skf
docker registry.

#### From source
first download the content from the github repository:

    git clone https://github.com/blabla1337/skf-apps

after downloading the source code change directory to the following folder:

    cd skf-apps/ASVS-16.5-CORS

Now build the docker image:

    sudo docker build -t cors-demo .

Now run the image:
    sudo docker run -p8080:8080 cors-demo

#### From SKF Docker registry

First download the image:

    sudo docker pull owasp-skf/ASVS-16.5-CORS

Than run the image:

    sudo docker run -p8080:8080 owasp-skf/ASVS-16.5-CORS

---
---

### Reconnaissance

| Recon | Description |
| --- | --- |
| Step 1| List all new or modified files  ![request/response](../img/req-resp-cors.png) \
\dfsdfsd 
\fdsfsdf 
\sdfdsf |
| Step 2| Show file differences that haven't been staged |

---
---

### Exploitation

#### Additional sources

[Portswigger blog](https://portswigger.net/blog/exploiting-cors-misconfigurations-for-bitcoins-and-bounties)
